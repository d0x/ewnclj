(ns ewnclj.game-engine
  (:require [clojure.pprint :as pp]
            [clojure.string :as str]
            [ewnclj.config :as c]
            [ewnclj.communication :as net]
            [ewnclj.board :as b]))

; ----- Response parsing
(defn parse-response [raw-response]
  "Zerlegt den raw-response in sender, code und message"
  (let [[full sender wuerfel code message] (re-matches #"(.*?) (.Würfel:.* )?(.*?)> (.*)" raw-response)]
    {:raw raw-response :sender sender :wuerfel wuerfel :code code :message message}))

; ----- Acting to responses
(defn apply-startaufstellung [game-state]
  game-state)

(defn parse-aufstellung [aufstellung]
  "Macht aus 311 512 113 221 422 631 ein Vektor aus steinen"
  (mapv b/parse-stein (str/split aufstellung #" ")))

(defn do-own-startaufstellung [game-state]
  (let [own-side (if (= (game-state :opponent-side) "t") "b" "t")
        aufstellung (if (= own-side "t") c/top-player-setup c/bot-player-setup)
        steine (parse-aufstellung aufstellung)
        new-board (reduce
                    (fn [board stein]
                      (b/bset board (stein :x) (stein :y) (str "b" (stein :augen))))
                    (game-state :board) steine)]
    (net/send-command aufstellung)
    (assoc game-state
      :own-side own-side
      :board new-board)))


(defn do-opponent-startaufstellung [game-state aufstellung]
  (let [steine (parse-aufstellung aufstellung)
        opponent-side (if (b/is-top-half (get steine 0)) "t" "b")
        new-board (reduce
                    (fn [board stein]
                      (b/bset board (stein :x) (stein :y) (str "o" (stein :augen))))
                    (game-state :board) steine)]
    (assoc game-state
      :opponent-side opponent-side
      :board new-board)))

(defn move [game-state]
  game-state)

(defn handle-Z [response game-state]
  (if (= (response :sender) "Server")
    (cond
      (= (response :message) "Sie sind am Zug") game-state
      (str/starts-with? (response :message) "Zug an ") game-state
      (str/starts-with? (response :message) "Würfel:") (if (:own-side game-state)
                                                         (move game-state)
                                                         (do-own-startaufstellung game-state))
      :else (println "Unhandled Response: " (response :raw)))
    (if (game-state :opponent-side)
      (move game-state)
      (do-opponent-startaufstellung game-state (response :message)))))

(defn handle-B [response game-state]
  "B - success"
  (if (= (:sender response) "Server")
    (do
      (cond
        (= (:message response) "Verbindung zum Server erstellt") (net/send-command (str "login " (game-state :botname)))
        (= (:message response) (str (game-state :botname) ", Sie sind angemeldet")) (println "Waiting for game-state requests")
        (= (:message response) "Spiel startet") game-state
        (= (:message response) "disconnect") (net/shutdown-network)
        :else (net/send-command "logout"))
      game-state)
    (throw (IllegalStateException. "Not implemented"))))

(defn handle-game-request [response game-state]
  "Akzeptiert jeden game-state Request"
  ;(println "Getting and accepting game-state request: " (response :message))
  (let [[full opponent rest] (re-matches #"(.*?) (.*)" (response :message))]
    (net/send-command "Ja")
    (assoc game-state :opponent-name opponent)))

(defn next-name [name]
  "Zählt am Namen die Zahl hoch. Also cb, cb1, cb2, cb3, ..."
  (str c/initial-bot-name
       (inc (Integer/parseInt (or (re-find #"\d+" name) "0")))))

(defn handle-nick-in-used [response game-state]
  (let [new-name (next-name (game-state :botname))]
    ;(println (game-state :botname) " is already in used, tyring " new-name " as next")
    (net/send-command (str "login " new-name))
    (assoc game-state :botname new-name)))

(defn handle-message [response game-state]
  ;(println "Recevied message from " (response :sender) ": " (response :message))
  game-state)

(defn handle-response [raw-response game-state]
  (let [response (parse-response raw-response)]
    ;(println "response: " response)
    (cond
      (= (response :code) "B") (handle-B response game-state)
      (= (response :code) "Q") (handle-game-request response game-state)
      (= (response :code) "M") (handle-message response game-state)
      (= (response :code) "E102") (handle-nick-in-used response game-state)
      (= (response :code) "E302") (net/shutdown-network)
      (= (response :code) "Z") (handle-Z response game-state))
    ))

(defn start-engine []
  (loop [game-state c/initial-game-state]
    (if (net/network-connected)
      (let [new-game-state (handle-response (net/read-response) game-state)]
        (pp/pprint (new-game-state :board))
        (Thread/sleep 2000)
        (recur new-game-state)))))

